package mail;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.NONE)
public class MailConfig {

	@XmlElement
	private boolean	smtpAuthentication;

	@XmlElement
	private boolean	starttls;

	@XmlElement
	private String	smtpHost;

	@XmlElement
	private String	port;

	@XmlElement
	private String	username;

	@XmlElement
	private String	password;

	@XmlElement
	private String	emailAddress;

	public MailConfig() {
	}

	public boolean isSmtpAuthentication() {
		return smtpAuthentication;
	}

	public boolean isStarttls() {
		return starttls;
	}

	
	public String getSmtpHost() {
		return smtpHost;
	}
	public String getPort() {
		return port;
	}
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	
	
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setStarttls(boolean starttls) {
		this.starttls = starttls;
	}
	
}
