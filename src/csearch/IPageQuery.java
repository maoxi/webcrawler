package csearch;

import java.util.ArrayList;

import org.jsoup.nodes.Document;

public interface IPageQuery<R extends Result> {

	public int getMaxTimeout();

	public String buildURL(int page, SortBy sortBy);

	public ArrayList<R> getResults(Document document);
}
