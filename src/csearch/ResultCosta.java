package csearch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class ResultCosta extends Result {

	private int duration;
	private int price;
	private String cabType;
	private ArrayList<String> habour = new ArrayList<>();
	private Calendar beginD = new GregorianCalendar();
	private Calendar endD = new GregorianCalendar();

	public ResultCosta(int duration, int price, String cabType,
			ArrayList<String> habour, Calendar beginD, Calendar endD) {
		super();
		this.duration = duration;
		this.price = price;
		this.cabType = cabType;
		this.habour = habour;
		this.beginD = beginD;
		this.endD = endD;
	}

	@Override
	public String toString() { // cabType=" + cabType + ",
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy"); // TODO
		return "duration=" + duration + " price=" + price + "\n begin="
				+ sdf.format(beginD.getTime()) + " end="
				+ sdf.format(endD.getTime()) + ", habour=" + habour + "\n";
	}

	public int getDuration() {
		return duration;
	}

	public int getPrice() {
		return price;
	}

	public String getCabType() {
		return cabType;
	}

	public ArrayList<String> getHabour() {
		return habour;
	}

	public Calendar getBeginD() {
		return beginD;
	}

	public Calendar getEndD() {
		return endD;
	}

}
