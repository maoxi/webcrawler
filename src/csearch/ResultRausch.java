package csearch;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ResultRausch extends Result{

	private String description;
	private Calendar beginD = new GregorianCalendar();
	private Calendar endD = new GregorianCalendar();
	
	public ResultRausch(String description) {
		super();
		this.description = description;
	}
	
	
	@Override
	public String toString() { //cabType=" + cabType + ",
		return "description=" + description + "\n";
	}

	public String getDescription() {
		return description;
	}
	public Calendar getBeginD() {
		return beginD;
	}
	public Calendar getEndD() {
		return endD;
	}
	
	
}
