package csearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Query extends ThreadPoolExecutor {

	private static Query instance = new Query();

	private Query() {
		super(1, 1, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>());
	}

	public static <R extends Result> void query(IPageQuery<R> query, int pages,
			Predicate<R> filter) {
		for (int page = 0; page < pages; page++) {
			final int final_page = page;
			instance.execute(new Runnable() {
				@Override
				public void run() {

					try {
						Document document = Jsoup
								.connect(
										query.buildURL(final_page, SortBy.Date)) 
										.userAgent("Mozilla/5.0")
								.timeout(query.getMaxTimeout()).get();
						ArrayList<R> results = query.getResults(document);
						List<Result> filtered = results.stream().filter(filter)
								.collect(Collectors.toList());
						System.out.println(filtered);
						InputStream stream = new ByteArrayInputStream(filtered
								.toString().getBytes(StandardCharsets.UTF_8));
						Files.copy(stream, Paths.get(".", "costoutput.txt"),
								StandardCopyOption.REPLACE_EXISTING);

					} catch (IOException e) {
						e.printStackTrace();
					}

					/*
					 * try { //use selium lib WebDriver wd = new ChromeDriver();
					 * wd.get("https://www.fahrstundenplaner.de/login");
					 * wd.findElement
					 * (By.id("mail")).sendKeys("maoxinyao1987@gmail.com");
					 * wd.findElement
					 * (By.id("password")).sendKeys("1987derauschmima");
					 * wd.findElement
					 * (By.cssSelector("button[type='submit']")).submit();
					 * wd.get(
					 * "https://www.fahrstundenplaner.de/calendar/718/90/2015-07-04"
					 * );
					 * 
					 * wd.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS)
					 * ;
					 * 
					 * Thread.sleep(2000); WebElement elem =
					 * wd.findElement(By.xpath("//*")); Document doc =
					 * Jsoup.parse(elem.getAttribute("outerHTML"));
					 * query.getResults(doc);
					 * 
					 * Calendar beginD = Calendar.getInstance(); DateFormat df =
					 * new SimpleDateFormat("yyyy-MM-dd"); ArrayList<R> results
					 * = new ArrayList<>(); for(int week=0; week< 6; week++){
					 * beginD.add(Calendar.WEEK_OF_MONTH, 1);
					 * System.out.println(
					 * df.format(beginD.getTime()).toString());
					 * 
					 * wd.get("https://www.fahrstundenplaner.de/calendar/718/90/"
					 * + df.format(beginD.getTime()).toString()); doc =
					 * Jsoup.parse
					 * (wd.findElement(By.xpath("/*")).getAttribute("outerHTML"
					 * )); results = query.getResults(doc); }
					 * 
					 * List<R> filtered = results .stream() .filter(filter) //
					 * Hier fuer dieses. .collect(Collectors.toList());
					 * System.out.println(filtered);
					 * 
					 * } catch (Exception e) { e.printStackTrace(); }
					 */
				}
			});
		}
		instance.shutdown();
	}
}
