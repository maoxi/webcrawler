package csearch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AidaQuery implements IPageQuery<ResultAida> {

	private Document document;
	private static Pattern cabineExtractor = Pattern.compile("\\w*kabine\\w*");
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

	@Override
	public int getMaxTimeout() {
		return 60000;
	}

	@Override
	public String buildURL(int page, SortBy sortBy) {
		StringBuilder sb = new StringBuilder();
		sb.append("https://www.aida.de/kreuzfahrt/angebote-buchen/reisesuche.18736.html?dyncat_catalog%5BcruiseListPage%5D=").append(page);
		return sb.toString();
	}

	@Override
	public ArrayList<ResultAida> getResults(Document document) {
		this.setDocument(document);

		ArrayList<ResultAida> results = new ArrayList<>();
		Elements rows = this.document.select(".result");
		for (Element row : rows) {
			if (row.select(".amount").isEmpty()) {
				continue;
			}

			int duration = Integer.parseInt(row.select(".duration>span").get(0)
					.text());

			String priceStr = row.select(".amount").get(0).text();
			int price = Integer.parseInt(priceStr);

			Matcher m = cabineExtractor.matcher(row.select("div.left").get(0)
					.text());
			String cabType;
			if (m.find()) {
				cabType = m.group();
			} else {
				cabType = "type not found";
			}

			ArrayList<String> habour = new ArrayList<>();
			String route = row.select(".result a").get(0).text();
			// Elements habours = row.select("div.ports");
			// for (Element aHabour : habours) {
			habour.add(route);
			// }


			Calendar beginD = new GregorianCalendar();
			Calendar endD = new GregorianCalendar();
			try {
				beginD.setTime(sdf.parse(row.select(".traveldate").get(0)
						.text().substring(0, 9)));
			} catch (ParseException e) {
				continue;
			}
			try {
				endD.setTime(sdf.parse(row.select(".traveldate").get(0).text()
						.substring(15)));
			} catch (ParseException e) {
				continue;
			}

			results.add(new ResultAida(duration, price, cabType, habour,
					beginD, endD));
		}
		return results;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Document getDocument() {
		return document;
	}

}
