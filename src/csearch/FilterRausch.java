package csearch;

import java.util.Calendar;
import java.util.function.Predicate;

public class FilterRausch implements Predicate<ResultRausch> {

	private Calendar beginD = null;
	private Calendar endD = null;

	@Override
	public boolean test(ResultRausch result) {
		if (beginD != null && result.getBeginD().before(beginD))
			return false;
		if (endD != null && result.getEndD().after(endD))
			return false;
		return true;
	}

	public void setBeginD(Calendar beginD) {
		this.beginD = beginD;
	}

	public void setEndD(Calendar endD) {
		this.endD = endD;
	}
}
