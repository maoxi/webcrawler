package csearch;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import mail.Mail;
import mail.MailConfig;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class RauschQuery implements IPageQuery<ResultRausch> {

	private static MailConfig mc = new MailConfig();
	private Document document;

	@Override
	public int getMaxTimeout() {
		return 30000;
	}

	@Override
	public String buildURL(int page, SortBy sortBy) {
		StringBuilder sb = new StringBuilder();
		sb.append("https://www.fahrstundenplaner.de/calendar/718/90/2015-07-04");
		return sb.toString();
	}

	public ArrayList<ResultRausch> getResults(Document document) {
		this.setDocument(document);
		this.print2File();
		
		ArrayList<ResultRausch> results = new ArrayList<>();
		mc.setEmailAddress("maoxinyao1987@gmail.com");
		mc.setPassword("1987degugemima");
		mc.setUsername("maoxinyao1987@gmail.com");
		mc.setPort("465");
		mc.setSmtpHost("smtp.gmail.com");

		Elements vormittag = document.select(".darkorange");
		Elements nachmittag = document.select(".green");
		
		if (!vormittag.isEmpty()) {
			for (Element freeLesson : vormittag) {
				results.add(new ResultRausch(freeLesson.select("a").first().attr("href")));
			}
		}
		if (!nachmittag.isEmpty()) {
			for (Element freeLesson : nachmittag) {
				results.add(new ResultRausch(freeLesson.select("a").first().attr("href")));
			}
		}

		System.out.println(results.toString());
		
		if (!results.isEmpty()) {
			//System.out.println(results.toString());
			Mail myMail = new Mail(mc);
			myMail.sendMail("maoxinyao1987@gmail.com","maoxinyao1987@gmail.com", "Fahrschule frei", results.toString());
		}
		return results;
	}

	public void print2File(){
		InputStream stream = new ByteArrayInputStream(this.document.toString().getBytes(StandardCharsets.UTF_8));
		try {
			Files.copy(stream, Paths.get(".", "output.html"), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("no file found");
		}
	}
	
	public void setDocument(Document document) {
		this.document = document;
	}
	public Document getDocument() {
		return document;
	}
}
