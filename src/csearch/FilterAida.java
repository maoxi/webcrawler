package csearch;

import java.util.Calendar;
import java.util.function.Predicate;

public class FilterAida implements Predicate<ResultAida>{
	
	private int minDuration = -1;
	private int maxDuration = -1;
	private int maxPrice = -1;
	private int pricePerDay = -1;
	private Calendar beginD = null;
	private Calendar endD = null;
	
	@Override
	public boolean test(ResultAida result) {
		
		if (minDuration != -1 && result.getDuration() < minDuration)
			return false;
		if (maxDuration != -1 && result.getDuration() > maxDuration)
			return false;
		if (maxPrice != -1 && result.getPrice() > maxPrice)
			return false;
		if (pricePerDay != -1
				&& result.getPrice() / result.getDuration() > pricePerDay)
			return false;
		if (beginD != null && result.getBeginD().before(beginD))
			return false;
		if (endD != null && result.getEndD().after(endD))
			return false;
		return true;
	}
	
	public void setMinDuration(int minDuration) {
		this.minDuration = minDuration;
	}

	public void setMaxDuration(int maxDuration) {
		this.maxDuration = maxDuration;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public void setPricePerDay(int pricePerDay) {
		this.pricePerDay = pricePerDay;
	}

	public void setBeginD(Calendar beginD) {
		this.beginD = beginD;
	}

	public void setEndD(Calendar endD) {
		this.endD = endD;
	}   
}
