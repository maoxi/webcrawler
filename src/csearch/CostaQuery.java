package csearch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CostaQuery implements IPageQuery<ResultCosta> {

	private Document document;

	@Override
	public String buildURL(int page, SortBy sortBy) {
		StringBuilder sb = new StringBuilder();
		sb.append("http://www.costakreuzfahrten.de/angebote-buchen/reisesuche.27776.html?");
		sb.append("screen=SearchBox&SearchButton=1&");
		sb.append("tx_aidadyncat_catalog%5BcruiseListPage%5D=").append(page)
				.append("&");
		sb.append("tx_aidadyncat_catalog%5BpaxConfig%5D=0&");
		sb.append("tx_aidadyncat_catalog%5BsearchState%5D=&");
		sb.append("tx_aidadyncat_catalog%5Bregion_or_ship%5D=region&");
		sb.append("tx_aidadyncat_catalog%5BsearchRegionShipCode%5D=*&");
		sb.append("tx_aidadyncat_catalog%5Bharbour_or_poc%5D=harbour&");
		sb.append("tx_aidadyncat_catalog%5BsearchPortCode%5D=*0&");
		sb.append("tx_aidadyncat_catalog%5BsearchStartDate%5D=&");
		sb.append("tx_aidadyncat_catalog%5BsearchEndDate%5D=&");
		sb.append("tx_aidadyncat_catalog%5BsearchEarlyBird%5D=*0&");
		sb.append("tx_aidadyncat_catalog%5BsearchPriceCode%5D=*0&");
		sb.append("tx_aidadyncat_catalog%5BsearchDurationCode%5D=*0&");
		sb.append("tx_aidadyncat_catalog%5BsearchSpecials%5D=*0&");
		sb.append("tx_aidadyncat_catalog%5BsearchTransportation%5D=&");

		switch (sortBy) {
		case Date:
			sb.append("tx_aidadyncat_catalog%5Bsorting%5D=").append("date")
					.append("&");
			break;
		case Price:
			sb.append("tx_aidadyncat_catalog%5Bsorting%5D=").append("price")
					.append("&");
			break;
		case Duration:
			sb.append("tx_aidadyncat_catalog%5Bsorting%5D=").append("duration")
					.append("&");
			break;
		default:
			throw new IllegalArgumentException();
		}
		sb.append("SearchButton=");
		return sb.toString();
	}

	@Override
	public int getMaxTimeout() {
		return 60000;
	}

	private static Pattern cabineExtractor = Pattern.compile("\\w*kabine\\w*"); // "\\w*kabine\\w*"
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");

	public ArrayList<ResultCosta> getResults(Document document) {
		this.setDocument(document);

		ArrayList<ResultCosta> results = new ArrayList<>();
		Elements rows = this.document.select(".result");
		for (Element row : rows) {
			if (row.select(".amount").isEmpty()) {
				continue;
			}

			int duration = Integer.parseInt(row.select(".duration>span").get(0)
					.text());

			String priceStr = row.select(".amount").get(0).text();
			String result = priceStr.replace(".", "");
			int price = Integer.parseInt(result);

			Matcher m = cabineExtractor.matcher(row.select("div.right").get(0)
					.text());
			String cabType;
			if (m.find()) {
				cabType = m.group();
			} else {
				cabType = "type not found";
			}

			ArrayList<String> habour = new ArrayList<>();
			//old
			/*Elements habours = row.select("div.ports a");
			for (Element aHabour : habours) {
				habour.add(aHabour.text());
			}*/
			//new
			habour.add(row.select("a.link-route").text());
			

			Calendar beginD = new GregorianCalendar();
			Calendar endD = new GregorianCalendar();
			try {
				String fromTill= row.select(".traveldate").get(0).text();
				String[] parts = fromTill.split("bis");
				beginD.setTime(sdf.parse(parts[0].replaceAll("\\s+","")));
				endD.setTime(sdf.parse(parts[1].replaceAll("\\s+","")));
			} catch (ParseException e) {
				continue;
			}
			
//			try {
//				endD.setTime(sdf.parse(row.select(".traveldate").get(1).text()));
//			} catch (ParseException e) {
//				continue;
//				// endD = null;
//			}

			// if(price/duration <50){
			results.add(new ResultCosta(duration, price, cabType, habour,
					beginD, endD));
			// }
		}
		return results;
	}

//	public void print2File() {
//		InputStream stream = new ByteArrayInputStream(this.document.toString()
//				.getBytes(StandardCharsets.UTF_8));
//		try {
//			Files.copy(stream, Paths.get(".", "output.html"),
//					StandardCopyOption.REPLACE_EXISTING);
//		} catch (IOException e) {
//			System.out.println("no file found");
//		}
//	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public Document getDocument() {
		return document;
	}
}
